# Welcome to ABC Kids Learning 👋

## Preview

<img width="250" height="450" style="float: left" src="https://drive.google.com/uc?export=view&id=1Y2X9brNNK4B-yOrWnLE0mdT783AD3ebI" />
<img width="250" height="450" style="float: left" src="https://drive.google.com/uc?export=view&id=1s-1tV5-of8Wtn3E2ImFoqz7dC6elEhJ7" />
<img width="250" height="450" style="float: left" src="https://drive.google.com/uc?export=view&id=15fDEihuFq55TZyUIv4IofkNGj0ipOC49" />

## Install

```sh
yarn
```

## Usage

```sh
yarn start
```

## Author

👤 **SuperDev**
